#!/usr/bin/env python3

# Copyright (C) 2022 Arm Ltd
# SPDX-License-Identifier: BSD-2-Clause

#  <xbar.title>GitLab CI Status</xbar.title>
#  <xbar.version>v1.0</xbar.version>
#  <xbar.author>Ross Burton <ross.burton@arm.com></xbar.author>
#  <xbar.desc>GitLab CI status</xbar.desc>
#  <xbar.dependencies>python, python-gitlab</xbar.dependencies>
#  <xbar.var>string(VAR_URL=""): GitLab API URL</xbar.var>
#  <xbar.var>string(VAR_PRIVATE_TOKEN=""): GitLab Private Token</xbar.var>
#  <xbar.var>number(VAR_PROJECT_ID=""): GitLab Project ID</xbar.var>


import gitlab
import enum
import os
import sys

verbose = False

gitlab_icon = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAllBMVEUAAAD0XSjiQyn8bCbjRCn8bCbjQyn8bSbiQyj7bCXjRC38oib8iyb8kybiQynhQinjQyn7oCb6bCb8oSb2ZSbiQinhQyjiRCr7WSTcOSjjRSnzriPjQynhQyn8bCX7bCX/cCbjRCnqRSr/dinuRivmRCnhQSn8bCbgPin/sCj/YCrnQCrqUyj0YyfvWyf9pib6gyb+aCY9rKcvAAAAIHRSTlMABP37+vfUwp1vC/n57uPf2MemmpN7X08/OiUWxsa4uPSDV30AAAC9SURBVBjTJY+HEoQgDESjqHd63V6uBAGxl///uQu6MLBL5g0JWJCe6SBZcE7BovVRwZED9Tal0pEZ3bQz6ZRUSaQkZiekTOjBq9U9AIuIu6o9gGKwbf7wnk/vwbk9FBD3TsvrXbx1+hhOgo2tvasdmTjBFxnOTcN508xkf+ALV+NEkU+oXeEDMS5DrZRG5hJB/4dCs21dN6ZFuPcDPuJ1Wa6IvommpfzWXS7dLTf+GKt6dd2rInPImCiCI/8BfkAQdXvV708AAAAASUVORK5CYII="

try:
    URL = os.environ["VAR_URL"]
    PRIVATE_TOKEN = os.environ["VAR_PRIVATE_TOKEN"]
    PROJECT_ID = os.environ["VAR_PROJECT_ID"]
except KeyError as e:
    print("❗ gitlab-ci")
    print("---")
    print(f"Parameter {e.args[0]} not set")
    sys.exit(0)


class Status(enum.IntEnum):
    manual = enum.auto()
    created = enum.auto()
    warning = enum.auto()  # Failed but allow_failure=True
    failed = enum.auto()
    running = enum.auto()
    success = enum.auto()
    canceled = enum.auto()
    skipped = enum.auto()
    pending = enum.auto()

    @classmethod
    def from_job(cls, job):
        status = Status[job.status]
        if status == Status.failed and job.allow_failure:
            return Status.warning
        return status

    def emoji(self):
        return {
            Status.manual: "👇",
            Status.created: "✨",
            Status.warning: "⚠️",
            Status.failed: "❗",
            Status.pending: "💤",
            Status.canceled: "🚫",
            Status.running: "🚀",
            Status.success: "✅",
            Status.skipped: "⏩",
        }[self.value]


def get_latest_pipeline(project, schedule):
    ref = schedule.ref.replace("refs/heads/", "")
    return project.pipelines.list(ref=ref, page=1, per_page=1)[0]


try:
    gl = gitlab.Gitlab(URL, private_token=PRIVATE_TOKEN)
    project = gl.projects.get(PROJECT_ID)
    if verbose:
        print(project, file=sys.stderr)

    worst_state = Status.success
    display_pipelines = {}
    for schedule in project.pipelineschedules.list(all=True):
        if verbose:
            print(schedule, file=sys.stderr)
        # Get the full schedule information, specfically the last pipeline
        schedule = project.pipelineschedules.get(schedule.id)
        pipeline = get_latest_pipeline(project, schedule)
        display_pipelines[schedule.description] = pipeline
        status = Status[pipeline.status]
        if status < worst_state:
            worst_state = status

    print(f"{Status.emoji(worst_state)} {project.name}")
    print("---")

    print(f"{project.name} | href={project.web_url} | image={gitlab_icon}")
    print("---")

    for name, pipeline in sorted(display_pipelines.items(), key=lambda p: p[0]):
        emoji = Status.emoji(Status[pipeline.status])
        print(f"{emoji} {name} | href={pipeline.web_url}")
        for job in sorted(pipeline.jobs.list(all=True), key=lambda job: job.name):
            emoji = Status.emoji(Status.from_job(job))
            print(f"-- {emoji} {job.name} | href={job.web_url}")

except Exception as e:
    print(f"⚠️ gitlab-ci")
    print("---")
    print(f"{e}|length=40")
